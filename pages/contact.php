<?php

include 'includes/dbConnection.php';

$query = "select * from jobs where j_active=1 order by j_id desc ";
$res = mysqli_query($link, $query) or die("can not select database");

?>

              <div class="col-md-7 col-md-offset-6 text-white">
                <div class="card">
					<div class="card-header">
                        <h4 class="mb-0 text-center text-danger">Contact Person Information</h4>
                    </div>
					<div class="card-body">
                        <form action="code/contactCode.php" class="form" method="post">
                        	<div class="form-group row">
                        		<label class="col-form-label form-control-label text-primary">Contact Name</label>
                        		<input class="form-control" type="text" name="name">
                        	</div>
                        	<div class="form-group row">
                        		<label class="col-form-label form-control-label text-primary">Contact Email</label>
                        		<input class="form-control" type="email" name="email">
                        	</div>
                        	<div class="form-group row">
                        		<label class="col-form-label form-control-label text-primary">Contact Mobile Number</label>
                        		<input class="form-control" type="number" name="mobile">
                        	</div>

                        	<input type="submit" class="btn btn-primary" value="Submit">
                        </form>
                    </div>
				</div>
                <br><br>
              </div>



