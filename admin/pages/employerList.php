<?php

include '../includes/dbConnection.php';

if (!($_SESSION['cat'] == 'employer' && $_SESSION['is_admin'] == 'Admin')) {
	header("location:.../index.php");
}

?>



              <br><br>
                  <legend class="text-center text-white">Recent Contact</legend>

                  <div class="table-responsive">
                    <table class="table table-bordered table-dark">
                    <thead>
                      <tr>
                        <th scope="col row">EmployerName</th>
                        <th scope="col">Mobile Number</th>
                        <th scope="col">Email</th>
                        <th scope="col">Is Admin</th>
                        <th scope="col">Approve Admin</th>
                      </tr>
                    </thead>

                    <tbody>
                            <?php
include '../includes/dbConnection.php';

$q = "select * from employers";
$res = mysqli_query($link, $q) or die("can not connect");

while ($row = mysqli_fetch_assoc($res)) {
	echo '
            <tr>
            <td>' . $row['er_fnm'] . '</td>
            <td>' . $row['er_mobile'] . '</td>
            <td>' . $row['er_email'] . '</td>
            <td>' . $row['is_admin'] . '</td>

        ';
	if ($row['is_admin'] == "Admin") {
		echo '<td><a href="changeToEmployer.php?id=' . $row['er_id'] . '"><input type="submit" class="btn btn-primary" value="UnApprove"></a></td>
            </tr>';
	} else {
		echo '<td><a href="changeToAdmin.php?id=' . $row['er_id'] . '"><input type="submit" class="btn btn-primary" value="Approve"></a></td>
            </tr>';
	}

}
?>
                    </tbody>
                  </table>
                  </div>

                <br><br>

