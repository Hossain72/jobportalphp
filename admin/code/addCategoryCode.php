<?php session_start();

if (!($_SESSION['cat'] == 'employer' && $_SESSION['is_admin'] == 'Admin')) {
	header("location:../index.php");
}

if (empty($_POST)) {
	exit;
}
$msg = array();

if (empty($_POST['cat_name'])) {
	$msg[] = "One of the field is empty";
}

if (!empty($msg)) {
	echo "<b>Errors:</b><br>";
	foreach ($msg as $k) {
		echo "<li>" . $k;
	}
} else {
	include '../includes/dbConnection.php';

	$cat_name = $_POST['cat_name'];

	$q = "insert into categories(cat_nm) values('$cat_name')";

	mysqli_query($link, $q) or die("wrong query");
	mysqli_close($link);
	header("location:../category.php");
}
?>