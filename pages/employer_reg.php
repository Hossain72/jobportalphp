
              <div class="col-md-4 col-md-offset-6 text-white">
              <br><br>
                <fieldset>
                  <legend class="text-center">Employer Register</legend>
                  <form action="code/employerRegCode.php" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" class="form-control form-control-sm" name="name" placeholder="Enter Your Name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control form-control-sm" name="email" placeholder="Enter Your email">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control form-control-sm" name="pwd" placeholder="Enter Password">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Company Name</label>
                        <input type="text" class="form-control form-control-sm" name="cname" placeholder="Enter Your Company Name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Company Address</label>
                        <textarea class="form-control" rows="3" name="caddress" placeholder="Enter Your Company Address"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Company Profile</label>
                        <textarea class="form-control" rows="3" name="cprofile" placeholder="Enter Your Company Profile"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number</label>
                        <input type="number" class="form-control form-control-sm" name="mobile" placeholder="Enter Mobile Number">
                      </div>

                      <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                  </form>
                </fieldset>
                <br><br>
              </div>
