
              <div class="col-md-7 col-md-offset-6 text-white">
                <div class="card">
						<div class="card-header">
                            <h4 class="mb-0 text-center text-danger">Sign In</h4>
                        </div>

                        <div class="card-body">
                        	<form action="code/loginCode.php" class="form" method="post">
                        		<div class="form-group row">
                        			<label class="col-form-label form-control-label text-primary">Email</label>
                        			<input class="form-control" type="email" name="email">
                        		</div>
                        		<div class="form-group row">
                        			<label class="col-form-label form-control-label text-primary">Password</label>
                        			<input class="form-control" type="password" name="pwd">
                        		</div>

                        		<div class="form-group row">
  									<label class="col-form-label form-control-label text-primary">Type</label>
  									<div class="col-sm-6 col-md-4">
    									<select id="company" name="cat" class="form-control">
      										<option value="employee">Employee</option>
      										<option value="employer">Employer</option>
    									</select>
  									</div>
								</div>

                        			<input type="submit" class="btn btn-primary" value="Login">
                        	</form>
                        </div>
					</div>
                <br><br>
              </div>
