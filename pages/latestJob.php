<div class="col-12">
                 <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-center text-danger">Latest Jobs</h4>
                </div>

                <div class="card-body">
                     <ol>
          <?php
include 'includes/dbConnection.php';

$result_per_page = 2;

$q = "select * from jobs where j_active=1";

$res = mysqli_query($link, $q) or die("Wrong Query");
$number_of_results = mysqli_num_rows($res);
$number_of_pages = ceil($number_of_results / $result_per_page);

if (!isset($_GET['page'])) {
	$page = 1;
} else {
	$page = $_GET['page'];
}

$this_page_first_result = ($page - 1) * $result_per_page;

$q = "select * from jobs where j_active=1 order by j_id desc limit " . $this_page_first_result .
	',' . $result_per_page;
$result = mysqli_query($link, $q) or die("Wrong Query");
while ($row = mysqli_fetch_assoc($result)) {

	echo '

<div class="col-9">
  <li class="border mb-2 pl-md-3">
    <h3 class="text-capitalize"><a href="job_details.php?id=' . $row['j_id'] . '">' . $row['j_title'] . '</a></h3>
    <p class=" text-info">Job Description: ' . $row['j_discription'] . '</p>
    <p class=" text-info">Job Experience: ' . $row['j_experience'] . '</p>
    <p class=" text-info">Job Salary: ' . $row['j_salary'] . '</p>
  </li>
</div>

                ';

}
echo '<br><br>';
for ($page = 1; $page <= $number_of_pages; $page++) {
	echo '<a href="index.php?page=' . $page . '"> <button type="button" class="btn btn-info">' . $page . '</button> </a>';
}

?>
          </ol>
                </div>
              </div>
              </div>