

              <div class="col-md-4 col-md-offset-6 text-white">
              <br><br>
                <fieldset>
                  <legend class="text-center">Employee Register</legend>
                  <form action="code/employeeRegCode.php" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text"  name="name" class="form-control form-control-sm" placeholder="Enter Your Name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control form-control-sm" placeholder="Enter Your email">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Gender</label>
                        <br>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="gender" value="male">Male
                          </label>
                        </div>

                         <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="gender" value="female">Female
                          </label>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                          <input type="password" name="pwd" class="form-control form-control-sm" placeholder="Enter Password">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <textarea class="form-control" name="address" rows="3" placeholder="Enter Your Address"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number</label>
                        <input type="number" name="mobile" class="form-control form-control-sm" placeholder="Enter Mobile Number">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Current Location</label>
                        <textarea class="form-control" name="cr_location" rows="3" placeholder="Enter Current Locaton"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Current Industry</label>
                        <input type="text" name="cr_industry" class="form-control form-control-sm" placeholder="Enter Current Industry">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Qualification</label>
                        <input type="text" name="qualification" class="form-control form-control-sm" placeholder="Enter Qualification">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Profile</label>
                        <textarea class="form-control" name="profile" rows="2" placeholder="Profile"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Resume</label>
                        <input type="file" name="resume" accept="application/pdf">
                      </div>

                        <button type="submit" class="btn btn-primary">Submit</button>

                  </form>
                </fieldset>
                <br><br>
              </div>
