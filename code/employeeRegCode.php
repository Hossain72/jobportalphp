<?php

if (empty($_POST)) {
	exit;
}
$msg = array();
if (empty($_POST['name']) || empty($_POST['gender']) || empty($_POST['email']) || empty($_POST['address']) ||
	empty($_POST['mobile']) || empty($_POST['cr_location']) ||
	empty($_POST['cr_industry']) || empty($_POST['qualification']) || empty($_POST['profile']) || empty($_POST['pwd'])) {
	$msg[] = "one of your field is empty";
}
if (strlen($_POST['pwd']) < 6) {
	$msg[] = "please enter atlist 6 digit password";
}

if (strlen($_POST['mobile']) != 11) {
	$msg[] = "please enter 11 digit mobile number";
}

if (empty($_FILES['resume']['name'])) {
	$msg[] = "Plz select a file";
}

if ($_FILES['resume']['error'] > 0) {
	$msg[] = "error uploading file";
}

if (file_exists("uploads/" . $_FILES['resume']['name'])) {
	$msg[] = "this file is already uploaded.";
}

if (!$_FILES['resume']['name'] == 'application/pdf') {
	$msg[] = "wrong file type";
}

if (!empty($msg)) {
	echo "<b> errors:</b><br>";
	foreach ($msg as $k) {
		echo "<li>" . $k;
	}
} else {
	include '../includes/dbConnection.php';

	$nm = $_POST['name'];
	$gender = $_POST['gender'];
	$email = $_POST['email'];
	$addr = $_POST['address'];
	$mobile = $_POST['mobile'];
	$cl = $_POST['cr_location'];
	$cas = $_POST['cas'];
	$cindustry = $_POST['cr_industry'];
	$quali = $_POST['qualification'];
	$profile = $_POST['profile'];
	$pwd = $_POST['pwd'];
	move_uploaded_file($_FILES['resume']['tmp_name'], "uploads/" . $_FILES['resume']['name']);
	$path = "uploads/" . $_FILES['resume']['name'];

	$q = "insert into employees(ee_resume,ee_pwd,ee_fnm,ee_gender,ee_email,ee_add,ee_mobileno,
		     ee_current_location,ee_annualsalary,ee_current_industry,ee_qualification,ee_profile)
	    values ('$path','$pwd','$nm','$gender','$email','$addr','$mobile','$cl','$cas','$cindustry','$quali','$profile')";

	mysqli_query($link, $q) or die("wrong query");
	mysqli_close($link);
	header("location:../employee_reg.php");
}
?>