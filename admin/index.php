<?php session_start();

error_reporting(0);

include 'includes/dbConnection.php';

if (!($_SESSION['cat'] == 'employer' && $_SESSION['is_admin'] == 'Admin')) {
	header("location:../index.php");
}

?>

<!DOCTYPE html>
<html lang="zxx">
<head>

  <?php
include 'includes/head.php';
?>

</head>
<body>
  <!-- Header section -->
  <header class="header-section">

    <?php
include 'includes/header.php';
?>
  </header>
  <!-- Header section end -->


  <!-- Page top section -->
  <section class="page-top-section set-bg" data-setbg="img/page-top-bg/3.jpg">
    <div class="page-info">

    </div>

  </section>
  <!-- Page top end-->



  <section class="blog-section spad">
    <div class="container">
      <div class="row">
        <div class="col-xl-9 col-lg-8 col-md-7">
          <div class="justify-content-center container center-block">
            <div class="row">
              <?php

if ($page == 'pages/category.php') {
	include 'pages/category.php';
} elseif ($page == 'pages/employerList.php') {
	include 'pages/employerList.php';
} elseif ($page == 'pages/contact.php') {
	include 'pages/contact.php';
} elseif ($page == 'pages/verify.php') {
	include 'pages/verify.php';
} elseif ($page == 'pages/jobs_by_category.php') {
	include 'pages/jobs_by_category.php';
} elseif ($page == 'pages/job_details.php') {
	include 'pages/job_details.php';
}

?>


            </div>
          </div>
        </div>
        <?php include 'includes/categories.php';?>
      </div>
    </div>
  </section>



  <div>
    <?php include 'includes/footer.php';?>
  </div>

</html>
