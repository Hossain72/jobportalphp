-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2019 at 05:56 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `a_id` int(4) NOT NULL,
  `a_uid` varchar(30) NOT NULL,
  `a_jid` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`a_id`, `a_uid`, `a_jid`) VALUES
(17, '11', '15'),
(18, '10', '18'),
(19, '10', '15'),
(20, '12', '19'),
(21, '12', '17'),
(22, '12', '20'),
(23, '10', '17'),
(24, '1', '24'),
(25, '1', '24');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(4) NOT NULL,
  `cat_nm` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_nm`) VALUES
(10, 'inport-export'),
(11, 'it-software'),
(12, 'it-hardware'),
(14, 'banking'),
(20, 'finance');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `cont_id` int(4) NOT NULL,
  `cont_fnm` varchar(30) NOT NULL,
  `cont_email` varchar(20) NOT NULL,
  `cont_mobile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`cont_id`, `cont_fnm`, `cont_email`, `cont_mobile`) VALUES
(25, 'Java', 'hossainanowar72@gmai', 1752867007);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `ee_id` int(4) NOT NULL,
  `ee_fnm` varchar(30) NOT NULL,
  `ee_pwd` varchar(10) NOT NULL,
  `ee_gender` varchar(1) NOT NULL,
  `ee_email` varchar(30) NOT NULL,
  `ee_add` varchar(300) NOT NULL,
  `ee_mobileno` varchar(10) NOT NULL,
  `ee_current_location` varchar(20) NOT NULL,
  `ee_annualsalary` int(10) NOT NULL,
  `ee_current_industry` varchar(20) NOT NULL,
  `ee_qualification` varchar(10) NOT NULL,
  `ee_profile` varchar(300) NOT NULL,
  `ee_resume` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`ee_id`, `ee_fnm`, `ee_pwd`, `ee_gender`, `ee_email`, `ee_add`, `ee_mobileno`, `ee_current_location`, `ee_annualsalary`, `ee_current_industry`, `ee_qualification`, `ee_profile`, `ee_resume`) VALUES
(1, 'Anowar Hossain', '123456', 'M', 'hossainanowar72@gmail.com', 'https://www.facebook.com/profile.php?id=100005828406477\r\nhttps://www.facebook.com/profile.php?id=100005828406477', '0175286700', 'Gazipur', 122, 'no', 'CSE', ' asafsadf', 'uploads/chapter 1.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `employers`
--

CREATE TABLE `employers` (
  `er_id` int(4) NOT NULL,
  `is_admin` enum('Employer','Admin') NOT NULL DEFAULT 'Employer',
  `er_fnm` varchar(30) NOT NULL,
  `er_pwd` varchar(10) NOT NULL,
  `er_company` varchar(30) NOT NULL,
  `er_add` varchar(100) NOT NULL,
  `er_mobile` int(11) DEFAULT NULL,
  `er_email` varchar(30) NOT NULL,
  `er_company_profile` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employers`
--

INSERT INTO `employers` (`er_id`, `is_admin`, `er_fnm`, `er_pwd`, `er_company`, `er_add`, `er_mobile`, `er_email`, `er_company_profile`) VALUES
(12, 'Admin', 'admin', '1111111', 'infosys', '\"infosys\",mumbai.', 112345678, 'infosys_company@gmail.com', 'abc'),
(13, 'Admin', 'riddhi', '1234567', 'infosys', 'infosys,mumbai.', 2147483647, 'riddhi@gmail.com', 'abc'),
(14, 'Admin', 'vishwa', '12345678', 'tcs', '\"tcs\",gandinagar.', 792143576, 'vishwa_patel@gmail.com', 'abc'),
(15, 'Employer', 'rushika', '12121212', 'patny', 'patnyy,gandhinagar.', 798765432, 'patny_company@yahoo.com', 'abc'),
(16, 'Employer', 'sagar', '2222222', 'tcs', 'tcs,ahemadabad', 792345677, 'sagar@gmail.com', 'dsfg'),
(17, 'Employer', 'Anowar Hossain', '123456', 'Grameen Bank', 'https://www.facebook.com/profile.php?id=100005828406477\r\nhttps://www.facebook.com/profile.php?id=100', 1752867007, 'hossainanowar72@gmail.com', 'dasfsdafs'),
(18, 'Admin', 'Anowar Hossain', '123456', 'askjfgsajgjh', 'https://www.facebook.com/profile.php?id=100005828406477\r\nhttps://www.facebook.com/profile.php?id=100', 1752867007, 'anowarhossainx2@yahoo.com', 'sfdv/.mfvnsdfnb '),
(22, 'Employer', 'Anowar Hossain Rifat', '123456', 'guwertgerlfj', 'https://www.facebook.com/profile.php?id=100005828406477\r\nhttps://www.facebook.com/profile.php?id=100', 1752867007, 'a@gmail.com', 'sjfabvjc nmnhesnv m');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `j_id` int(4) NOT NULL,
  `j_category` varchar(40) NOT NULL,
  `j_owner_name` varchar(30) NOT NULL,
  `j_title` varchar(30) NOT NULL,
  `j_hours` float(3,1) NOT NULL,
  `j_salary` int(10) NOT NULL,
  `j_experience` int(3) NOT NULL,
  `j_discription` varchar(300) NOT NULL,
  `j_city` varchar(20) NOT NULL,
  `j_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`j_id`, `j_category`, `j_owner_name`, `j_title`, `j_hours`, `j_salary`, `j_experience`, `j_discription`, `j_city`, `j_active`) VALUES
(15, 'it-software', 'riddhi', 'need for the manager', 10.0, 40000, 2, 'abc', 'pune', 1),
(17, 'banking', 'riddhi', 'need for the ca', 8.0, 45000, 4, 'dff', 'ahmedabad', 1),
(18, 'it-hardware', 'vishwa', 'need for the manager', 10.0, 34000, 2, 'fdrf', 'pune', 1),
(19, 'inport-export', 'vishwa', 'need for thr tredar', 12.0, 25000, 3, 'sdsdf', 'rajkot', 1),
(20, 'finance', 'rushika', 'need for the assistent', 8.0, 34667, 3, 'wrert', 'pune', 1),
(21, 'banking', 'rushika', 'need for the manager', 10.0, 45000, 5, 'dsd', 'kolkota', 1),
(22, 'inport-export', 'riddhi', 'need for the assistent', 12.0, 10000, 1, 'abv', 'porbunder', 1),
(23, 'it-software', '', 'Java Post', 8.0, 20000, 2, 'dghlkfasdklvnafsi;o', 'Gazipur', 1),
(24, 'it-software', '', 'PHP post', 7.0, 15000, 2, 'dghlkfasdklvnafsi;o', 'Gazipur', 1),
(25, 'it-software', 'Anowar Hossain', 'Java Post', 8.0, 15000, 1, 'Java Basic', 'Gazipur', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`ee_id`);

--
-- Indexes for table `employers`
--
ALTER TABLE `employers`
  ADD PRIMARY KEY (`er_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`j_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `a_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `cont_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `ee_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employers`
--
ALTER TABLE `employers`
  MODIFY `er_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `j_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
