<?php session_start();

include 'includes/dbConnection.php';

$query = "select * from jobs where j_active=1 order by j_id desc ";
$res = mysqli_query($link, $query) or die("can not select database");

?>
<!DOCTYPE html>
<html lang="zxx">
<head>

	<?php
include 'includes/head.php';
?>

</head>
<body>
	<!-- Header section -->
	<header class="header-section">

		<?php
include 'includes/header.php';
?>
	</header>
	<!-- Header section end -->


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/3.jpg">
		<div class="page-info">
			<?php

if (isset($_SESSION['employee'])) {
	echo '<h2 class="title text-center">Hello ' . $_SESSION['ee_name'] . '</h2>';
} elseif (isset($_SESSION['employer'])) {
	echo '<h2 class="title text-center">Hello ' . $_SESSION['er_name'] . '</h2>';
}

?>
		</div>

	</section>
	<!-- Page top end-->

	<section class="blog-section spad">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-7">

					<?php

if (isset($_POST['search'])) {

	$search = $_POST['search'];

	$query = "SELECT * FROM jobs WHERE j_title LIKE '%$search%' and j_active=1";
	$search_query = mysqli_query($link, $query) or die("wrong query");

	$count = mysqli_num_rows($search_query);

	if ($count == 0) {
		echo "<h1>Result not found.</h1>";
	} else {

		while ($row = mysqli_fetch_assoc($search_query)) {

			$j_title = $row['j_title'];
			?>


                <h2>
                    <a href="#"><?php echo $j_title ?></a>
                </h2>

                <?php }
	}

}?>


				</div>
				<?php include 'includes/categories.php';?>
			</div>
		</div>
	</section>



	<div>
		<?php include 'includes/footer.php';?>
	</div>

</html>
