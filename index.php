<?php session_start();
error_reporting(0);
include 'includes/dbConnection.php';

?>
<!DOCTYPE html>
<html lang="zxx">
<head>

	<?php
include 'includes/head.php';
?>

</head>
<body>
	<!-- Header section -->
	<header class="header-section">

		<?php
include 'includes/header.php';
?>
	</header>
	<!-- Header section end -->


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/3.jpg">
		<div class="page-info">
			<?php

if (isset($_SESSION['employee'])) {
	echo '<h2 class="title text-center">Hello ' . $_SESSION['ee_name'] . '</h2>';
} elseif (isset($_SESSION['employer'])) {
	echo '<h2 class="title text-center">Hello ' . $_SESSION['er_name'] . '</h2>';
} else {
	echo '<h2 class="title text-center">Welcome Job Portal System</h2>';
}

?>
		</div>

	</section>
	<!-- Page top end-->

	<section class="blog-section spad">
    <div class="container">
      <div class="row">
        <div class="col-xl-9 col-lg-8 col-md-7">
          <div class="justify-content-center container center-block">
            <div class="row">
              <?php

if ($page == 'pages/login.php') {
	include 'pages/login.php';
} elseif ($page == 'pages/employee_reg.php') {
	include 'pages/employee_reg.php';
} elseif ($page == 'pages/employer_reg.php') {
	include 'pages/employer_reg.php';
} elseif ($page == 'pages/contact.php') {
	include 'pages/contact.php';
} elseif ($page == 'pages/post_job.php') {
	include 'pages/post_job.php';
} elseif ($page == 'pages/manage_job.php') {
	include 'pages/manage_job.php';
} elseif ($page == 'pages/show.php') {
	include 'pages/show.php';
} elseif ($page == 'pages/jobs_by_category.php') {
	include 'pages/jobs_by_category.php';
} elseif ($page == 'pages/job_details.php') {
	include 'pages/job_details.php';
} else {
	include 'pages/latestJob.php';
}

?>


            </div>
          </div>
        </div>
        <?php include 'includes/categories.php';?>
      </div>
    </div>
  </section>


	<div>
		<?php include 'includes/footer.php';?>
	</div>

</html>



